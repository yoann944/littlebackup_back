import app from './app';

//open chat model messages
import { Message } from 'models/chat-models';

let http = require('http');
let server = http.Server(app);

// socket io chat imports
let socketIO = require('socket.io');
let io = socketIO(server);
const PORT = process.env.PORT || 3000;

server.listen(PORT, () => {
    console.log(`Express server listening on port ${PORT}`);
    // notify when people join / leave the websocket
    io.on('connect', (socket: any) => {
        console.log('Connected client on port %s.', PORT);
        socket.on('message', (m: Message) => {
            console.log('[server](message): %s', JSON.stringify(m));
            io.emit('message', m);
        });

        socket.on('disconnect', () => {
            console.log('Client disconnected');
        });
    });
})
