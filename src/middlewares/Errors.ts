export enum ErrorCodes {
    Auth_Token_Expired = 0,
    Auth_Token_Invalid = 1,
    Auth_Token_Missing = 2,
    User_Login_Wrong_Credentials = 3,
    Client_Invalid_Parameters = 4,
    Client_Missing_Parameters = 5,
    Client_Forbidden = 6
  }
  