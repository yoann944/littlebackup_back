import * as express from "express";
import * as bodyParser from "body-parser";
import * as mongoose from "mongoose";
import * as settings from './config/settings';

// jwt token import
import * as expressJWT from 'express-jwt';

// routes imports
import { UserRoutes } from "./routes/userRoutes"
import { CoursRoutes } from "./routes/coursRoutes"
import { ThemeRoutes } from "./routes/themeRoutes"
import { LevelRoutes } from "./routes/levelRoutes"
import { MessageRoutes } from "./routes/messageRoutes"
import { AuthenticationRoutes } from "./routes/authenticationRoutes";

class App {

    public app: express.Application;

    public userRoute: UserRoutes = new UserRoutes();
    public coursRoutes: CoursRoutes = new CoursRoutes();
    public themeRoutes: ThemeRoutes = new ThemeRoutes();
    public messageRoutes: MessageRoutes = new MessageRoutes();
    public authenticationRoutes: AuthenticationRoutes = new AuthenticationRoutes();
    // public levelRoutes: LevelRoutes = new LevelRoutes();

    // secret token key
    private readonly publicKey: string = settings.default.token.secret;

    // database connexion
    public mongoUrl: string = `${settings.default.database.type}://${settings.default.database.user}:${settings.default.database.password}@${settings.default.database.server}:${settings.default.database.port}/${settings.default.database.name}`;

    constructor() {
        this.app = express();

        this.app.use(function (req, res, next) {
            res.setHeader('Access-Control-Allow-Origin', '*');
            res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
            res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');
            next();
        });

        this.config();        
        this.mongoSetup();
        this.secureAPIMethod();
        this.userRoute.routes(this.app);
        this.coursRoutes.routes(this.app);
        this.themeRoutes.routes(this.app);
        this.messageRoutes.routes(this.app);
        this.authenticationRoutes.routes(this.app);
    }

    private config(): void {
        this.app.use(bodyParser.json());
        this.app.use(bodyParser.urlencoded({ extended: false }));
        this.app.use(express.static('public'));
    }

    private mongoSetup(): void {
        mongoose.Promise = global.Promise;
        mongoose.connect(this.mongoUrl);
    }

    private secureAPIMethod() {
        this.app.use(expressJWT({ secret: this.publicKey }).unless({ path: ['/authentication'] }));
    }
}

export default new App().app;