import * as mongoose from 'mongoose';
import { CoursSchema } from '../models/coursModel';
import { Request, Response } from 'express';

const Cours = mongoose.model('Cours', CoursSchema);

export class CoursController{

    public addNewCours (req: Request, res: Response) {      
        
        console.log(req.body);
        
        let newCours = new Cours(req.body);
    
        newCours.save((err, cours) => {
            if(err){
                res.send(err);
            }     
            res.json(cours);
        });
    }

    public getCours (req: Request, res: Response) {           
        Cours.find({}, (err, cours) => {
            if(err){
                res.send(err);
            }
            res.json(cours);
        });
    }

    public getCoursWithID (req: Request, res: Response) {           
        Cours.findById(req.params.coursId, (err, cours) => {
            if(err){
                res.send(err);
            }
            res.json(cours);
        });
    }

    public updateCours (req: Request, res: Response) {           
        Cours.findOneAndUpdate({ _id: req.params.coursId }, req.body, { new: true }, (err, cours) => {
            if(err){
                res.send(err);
            }
            res.json(cours);
        });
    }

    public deleteCours (req: Request, res: Response) {           
        Cours.remove({ _id: req.params.coursId }, (err, cours) => {
            if(err){
                res.send(err);
            }
            res.json({ message: 'Successfully deleted Cours!'});
        });
    }
    
}