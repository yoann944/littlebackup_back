import * as mongoose from 'mongoose';
import { UserSchema } from '../models/userModel';
import { Request, Response } from 'express';
import { handlePasswords } from '../helpers/hashingPasswords';
import { session } from '../helpers/session';

const User = mongoose.model('User', UserSchema);

export class AuthenticationController {

    public login(req: Request, res: Response) {
        console.log(req.body);
        if (!req.body.password || !req.body.email) {
            res.status(400).send({ message: 'Missing parameters' });
        } else {
            User.findOne({ email: req.body.email }, (err, user) => {
                if (err) res.send(err);
                if (user) {
                    if (req.body.password === user.password) {
                        let token = session.createToken(user);
                        res.send({ user, token });
                    } else {
                        res.status(404).send({ message: 'Veuillez vérifier vos identifiants' });
                    }
                } else {
                    res.status(404).send({ message: 'Veuillez vérifier vos identifiants' });
                }
            });
        }

    }

    public getUserByToken(req: Request, res: Response) {
        User.findOne({ accessToken: req.params.token }, (err, user) => {
            if (err) res.send(err);
            res.send(user);
        })
    }

}
