import * as mongoose from 'mongoose';
import { Request, Response } from 'express';

import { LevelSchema } from '../models/levelModel';

const Level = mongoose.model('Level', LevelSchema);

export class LevelController{

    public addNewLevel (req: Request, res: Response) {                
        let level = new Level(req.body);
    
        level.save((err, level) => {
            if(err){
                res.send(err);
            }    
            res.json(level);
        });
    }

    public getLevels (req: Request, res: Response) {           
        Level.find({}, (err, level) => {
            if(err){
                res.send(err);
            }
            res.json(level);
        });
    }

    public getLevelWithID (req: Request, res: Response) {           
        Level.findById(req.params.levelId, (err, level) => {
            if(err){
                res.send(err);
            }
            res.json(level);
        });
    }

    public updateLevel (req: Request, res: Response) {           
        Level.findOneAndUpdate({ _id: req.params.levelId }, req.body, { new: true }, (err, level) => {
            if(err){
                res.send(err);
            }
            res.json(level);
        });
    }

    public deleteLevel (req: Request, res: Response) {           
        Level.remove({ _id: req.params.userId }, (err, level) => {
            if(err){
                res.send(err);
            }
            res.json({ message: 'Successfully deleted Level!'});
        });
    }
    
}