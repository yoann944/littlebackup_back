import * as mongoose from 'mongoose';
import { ThemeSchema } from '../models/themeModel';
import { Request, Response } from 'express';

const Theme = mongoose.model('Level', ThemeSchema);

export class ThemeController{

    public addNewTheme (req: Request, res: Response) {                
        let newUser = new Theme(req.body);
    
        newUser.save((err, theme) => {
            if(err){
                res.send(err);
            }    
            res.json(theme);
        });
    }

    public getThemes (req: Request, res: Response) {           
        Theme.find({}, (err, theme) => {
            if(err){
                res.send(err);
            }
            res.json(theme);
        });
    }

    public getThemeWithID (req: Request, res: Response) {           
        Theme.findById(req.params.levelId, (err, theme) => {
            if(err){
                res.send(err);
            }
            res.json(theme);
        });
    }

    public updateTheme (req: Request, res: Response) {           
        Theme.findOneAndUpdate({ _id: req.params.themeId }, req.body, { new: true }, (err, theme) => {
            if(err){
                res.send(err);
            }
            res.json(theme);
        });
    }

    public deleteTheme (req: Request, res: Response) {           
        Theme.remove({ _id: req.params.userId }, (err, theme) => {
            if(err){
                res.send(err);
            }
            res.json({ message: 'Successfully deleted theme!'});
        });
    }
    
}