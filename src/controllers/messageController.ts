import * as mongoose from 'mongoose';
import { MessageSchema } from '../models/messageModel';
import { Request, Response } from 'express';

const Message = mongoose.model('Message', MessageSchema);

export class MessageController{

    public addNewMessage (req: Request, res: Response) {                
        let newUser = new Message(req.body);
    
        newUser.save((err, msg) => {
            if(err){
                res.send(err);
            }    
            res.json(msg);
        });
    }

    public getMessages (req: Request, res: Response) {           
        Message.find({}, (err, msgs) => {
            if(err){
                res.send(err);
            }
            res.json(msgs);
        });
    }

    public getMessageWithID (req: Request, res: Response) {           
        Message.findById(req.params.levelId, (err, msg) => {
            if(err){
                res.send(err);
            }
            res.json(msg);
        });
    }

    public updateMessage(req: Request, res: Response) {           
        Message.findOneAndUpdate({ _id: req.params.msgId }, req.body, { new: true }, (err, msg) => {
            if(err){
                res.send(err);
            }
            res.json(msg);
        });
    }

    public deleteMessage (req: Request, res: Response) {           
        Message.remove({ _id: req.params.msgId }, (err, msg) => {
            if(err){
                res.send(err);
            }
            res.json({ message: 'Successfully deleted msg!'});
        });
    }
    
}