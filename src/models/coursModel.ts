import * as mongoose from 'mongoose';

const Schema = mongoose.Schema;

export const CoursSchema = new Schema({
    title: {
        type: String,
        required: 'Enter a title'
    },
    description: {
        type: String,
        required: 'Enter a description'
    },
    urlVideo: {
        type: String,
        required: 'Enter video url'
    },
    urlVignette: {
        type: String,
        required: 'Enter url vignette'
    },
    isSeen: {
        type: Boolean,
        default: false
    },
    courJour: {
        type: Boolean,
        default: false
    },
    levelID: {
        type: Number,
    },
    
    updated_date: {
        type: Date,
        default: Date.now
    },
    created_date: {
        type: Date,
        default: Date.now
    }
});