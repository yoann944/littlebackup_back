import * as mongoose from 'mongoose';

const Schema = mongoose.Schema;

export const ThemeSchema = new Schema({
    text: {
        type: String,
    },
    label: {
        type: String,
    },

    updated_date: {
        type: Date,
        default: Date.now
    },
    created_date: {
        type: Date,
        default: Date.now
    }
});