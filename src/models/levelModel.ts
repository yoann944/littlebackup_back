import * as mongoose from 'mongoose';

const Schema = mongoose.Schema;

export const LevelSchema = new Schema({
    label: {
        type: String,
        required: 'Enter a label'
    },

    updated_date: {
        type: Date,
        default: Date.now
    },
    created_date: {
        type: Date,
        default: Date.now
    }
});