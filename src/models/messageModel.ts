import * as mongoose from 'mongoose';

const Schema = mongoose.Schema;

export const MessageSchema = new Schema({
    text: {
        type: String,
    },
    idUser: {
        type: String,
    },

    updated_date: {
        type: Date,
        default: Date.now
    },
    created_date: {
        type: Date,
        default: Date.now
    }
});