// import {Request, Response, NextFunction} from "express";
import { ThemeController } from "../controllers/themeController";

export class ThemeRoutes { 
    
    public themeController: ThemeController = new ThemeController() 
    
    public routes(app): void {   
        
        app.route('/themes')
        .get(this.themeController.getThemes)        
        .post(this.themeController.addNewTheme);

        app.route('/themes/:themeId')
        .get(this.themeController.getThemeWithID)
        .put(this.themeController.updateTheme)
        .delete(this.themeController.deleteTheme)
    }
}