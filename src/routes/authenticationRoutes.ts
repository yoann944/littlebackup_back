import {Request, Response, NextFunction} from "express";
import { AuthenticationController } from "../controllers/authenticationController";

export class AuthenticationRoutes { 
    
    public authenticationController: AuthenticationController = new AuthenticationController() 
    
    public routes(app): void {

        app.route('/authentication/:token')
        .get(this.authenticationController.getUserByToken);

        app.route('/authentication')
        // Post login and password
        .post(this.authenticationController.login);
    }
}
