import {Request, Response, NextFunction} from "express";
import { CoursController } from "../controllers/coursController";

export class CoursRoutes { 
    
    public userController: CoursController = new CoursController() 
    
    public routes(app): void {   
        

        app.route('/cours')
        .get(this.userController.getCours)        
        .post(this.userController.addNewCours);

        app.route('/cours/:coursId')
        .get(this.userController.getCoursWithID)
        .put(this.userController.updateCours)
        .delete(this.userController.deleteCours)
    }
}