import {Request, Response, NextFunction} from "express";
import { LevelController } from "../controllers/levelController";

export class LevelRoutes { 

    public levelController: LevelController = new LevelController();
    
    public routes(app): void { 

        app.route('/levels')
        .get(this.levelController.getLevels)      
        .post(this.levelController.addNewLevel);

        app.route('/levels/:levelId')
        .get(this.levelController.getLevelWithID)
        .put(this.levelController.updateLevel)
        .delete(this.levelController.deleteLevel)
    }
}