// import {Request, Response, NextFunction} from "express";
import { MessageController } from "../controllers/messageController";

export class MessageRoutes { 
    
    public messageController: MessageController = new MessageController() 
    
    public routes(app): void {   
        
        app.route('/messages')
        .get(this.messageController.getMessages)        
        .post(this.messageController.addNewMessage);

        app.route('/messages/:messageId')
        .get(this.messageController.getMessageWithID)
        .put(this.messageController.updateMessage)
        .delete(this.messageController.deleteMessage)
    }
}