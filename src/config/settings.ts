export default {
    info: 'this file should not be versioned',
    database: {
        type: 'mongodb',
        name: 'littlebackup',
        port: 45512,
        user: 'littlebackupuser',
        password: 'lbu1230',
        server: 'ds245512.mlab.com'
    },
    token: {
        secret: 'iamthesupersecretkeythatnooneshouldknowabout',
        expiresIn: 86400
    },
    urlToken: {
        expiresIn: 86400 * 365 * 6
    },
    password: {
        salt: 'iamthesupersaltkeythatnooneshouldknowabout'
    },
}