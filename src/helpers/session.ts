import * as JWT from 'jsonwebtoken';
import settings from '../config/settings';
import { ErrorCodes } from '../middlewares/Errors';

export const session = {
	createToken(user) {
		return JWT.sign({ id: user.id, role: user.role }, settings.token.secret, {
			expiresIn: settings.token.expiresIn
		});
	},
	createURLToken(user) {
		return JWT.sign({ id: user.id, role: user.role }, settings.token.secret, {
			expiresIn: settings.token.expiresIn
		});
	},
	checkToken(token: string): Promise<{ id: string, role: string }> {
		return new Promise((resolve, reject) => {
			JWT.verify(token, settings.token.secret, (err, decoded) => {
				if (err) {
					reject(new Error(err.name === 'TokenExpiredError'
						? ErrorCodes[ErrorCodes.Auth_Token_Expired]
						: ErrorCodes[ErrorCodes.Auth_Token_Invalid]));
				}
				resolve(decoded);
			});
		});
	}
};
