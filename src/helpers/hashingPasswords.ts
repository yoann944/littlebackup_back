import * as crypto from 'crypto';
import settings from '../config/settings';

const hashPassword = (password) => crypto.createHash('sha1').update(settings.password.salt + password).digest('hex');

const checkPassword = (password, hashedPassword): boolean => hashedPassword === hashPassword(password);

export const handlePasswords = {
	hashPassword,
	checkPassword
};
